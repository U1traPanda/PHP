<?php

declare(strict_types=1);

use EventList\Events;

require_once './vendor/autoload.php';

if (strcasecmp($_SERVER['PATH_INFO'], '/send') === 0) {
    $events = new Events();
    if (isset($_GET["id"], $_GET["event"], $_GET["time"]) && (is_numeric($_GET["id"]))) {
        $events->saveEvent((int)$_GET["id"], $_GET["event"], $_GET ["time"]);
    }
    else echo 'Не хватает параметров, либо ID не является числом';
}


if (strcasecmp($_SERVER['PATH_INFO'], '/check') === 0) {
    $events = new Events();

    if (isset($_GET["id"]) && (is_numeric($_GET["id"]))){
        $result = $events->getEventsByID((int)$_GET["id"]);
        print_r(json_encode($result, JSON_PRETTY_PRINT));
    }
    else echo "Неверный ID";
}