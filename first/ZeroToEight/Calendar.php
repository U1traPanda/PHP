<?php

namespace ZeroToEight;
class Calendar
{
    public function getDays()
    {
        $today = getdate();
        $monthDays = cal_days_in_month(CAL_GREGORIAN, $today['mon'], $today['year']);
        return $monthDays - $today['mday'];
    }

    public function getPidor()
    {
        return 'pidor';
    }
}