<?php
//Сделайте функцию, которая параметром будет год и проверять, високосный он или нет.
function checkYear(int $year)
{
    return $year % 4 === 0 && $year % 100 !== 0
    || $year % 100 === 0 && $year % 400 === 0;
}

//if(checkYear(2024) === true) {
//    echo 'Високосный год';
//} else echo 'Обычный год';

echo checkYear(2024) === true ? 'Високосный год' : 'Обычный год';