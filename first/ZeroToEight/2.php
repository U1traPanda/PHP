<?php
//Дано некоторое число: 12345 Переверните его: 54321
$newNumber = 12345;
$textNumber = (string)$newNumber;
$arrayNumber = str_split($textNumber);
$reverseNumber = '';

for ($i = count($arrayNumber) - 1; $i >= 0; $i--){
    $reverseNumber = $reverseNumber . $arrayNumber[$i];
}

echo $reverseNumber;