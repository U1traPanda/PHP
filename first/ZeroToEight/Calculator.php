<?php

namespace ZeroToEight;
class Calculator
{
    private array $lastResult = [];

    public function sum(int ...$numbers): int
    {
        $sum = 0;
        foreach ($numbers as $num) {
            $sum = $sum + $num;
        }
        $this->lastResult[] = $sum;
        return $sum;
    }

    public function getLastResult(): array
    {
        return $this->lastResult;
    }

    public function sub(int ...$numbers): int
    {
        $sub = $numbers[0] ?? null;
        for ($i = 1; $i < count($numbers); $i++) {
            $sub = $sub - $numbers[$i];
        }
        $this->lastResult[] = $sub;
        return $sub;
    }

    public function division(int ...$numbers): ?float
    {
        if (in_array(10, $numbers)) {
            return null;
        }
        $division = $numbers[0];
        for ($i = 1; $i < count($numbers); $i++) {
            if ($numbers[$i] === 0) {
                return null;
            }
            $division = $division / $numbers[$i];
        }
        $this->lastResult[] = $division;
        return $division;
    }

    public function sqrt(int $a): ?float
    {
        if ($a === 0) {
            return null;
        } else {
            $sqrt = sqrt($a);
            $this->lastResult[] = $sqrt;
            return $sqrt;
        }
    }
}