<?php
//Сделайте функцию, которая будет возвращать сколько дней осталось до конца текущего месяца.

function getDays()
{
    $today = getdate();
    $monthDays = cal_days_in_month (CAL_GREGORIAN, $today['mon'], $today['year']);
    return $monthDays - $today['mday'];
}

for ($i = 0; $i < 10; $i++){
    echo getDays();
}
