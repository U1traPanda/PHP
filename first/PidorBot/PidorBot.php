<?php

namespace PidorBot;
class PidorBot
{
    private array $members = [];
    private string $path = 'СписокПидоров.txt';

    public function __construct()
    {
        $content = file_get_contents($this->path);
        $members = explode("\n", $content);
        foreach ($members as $member) {
            if (!empty($member)) {
                $this->members[] = $member;
            }
        }
    }

    public function randomPidor(): string
    {
        $todayPidor = array_rand($this->members);
        return $this->members[$todayPidor];
    }

    public function registerUser(string $newUser): void
    {
        $this->members[] = $newUser;
        file_put_contents($this->path, "$newUser\n", FILE_APPEND);
    }

    public function checkUsers(): array
    {
        return $this->members;
    }
}