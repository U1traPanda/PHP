<?php

declare(strict_types=1);

namespace EventList;

class Events
{
    private string $path = __DIR__.'/EventList.txt';

    private array $array;

    public function __construct() {
        if (file_exists($this->path)){
            $content = file_get_contents($this->path);
            $this->array = json_decode($content) ?? []; //если json_decode($content) === null, то  $this->array пишется []
        }
        else $this->array = [];
    }

    public function saveEvent(int $id, string $event, string $time): void
    {
            $this->array[] = [
                'id' => $id,
                'event' => $event,
                'time' => $time,
            ];

            $eventDetails = json_encode($this->array, JSON_PRETTY_PRINT);
            file_put_contents($this->path, $eventDetails);
    }

    public function getEventsByID(int $id): array
    {
        if (empty($this->array)){
            return [];
        }
        $eventList = [];
        foreach ($this->array as $value) {
            if ($value->id === $id) {
                $eventList[] = [
                    'id' => $value->id,
                    'event' => $value->event,
                    'time' => $value->time,
                ];
            }
        }
        usort($eventList, function($event1, $event2){
            if ($event1['time'] === $event2['time']) return  0;
            return ($event1['time'] > $event2['time']) ? -1 :  1;
        });
        return $eventList;
    }
}